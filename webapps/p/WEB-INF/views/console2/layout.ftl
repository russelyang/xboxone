[#import "/nexus.ftl" as nexus]
[#import "/nexfn.ftl" as nexfn]
[#import "/spring.ftl" as spring]

[#macro connect title="" javascripts="" stylesheets="" manifest=""]
<!DOCTYPE html>
<html lang="${.lang}" [#if manifest?has_content]manifest=${manifest}[/#if]>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=${.output_encoding}"/>

    <title>${title}</title>
    <meta name="title" content="[@nexus.message "console" "connect_login_header"/]"/>
    <meta name="description" content="[@nexus.message "console" "connect_login_description"/]"/>
    <link rel="shortcut icon" href="[@spring.url "/favicon.ico"/]"/>
    <meta name="lc" content="${.locale}"/>

    [@nexus.js "ui/core" "js/jquery-1.8.3.min.js"/]
    [@nexus.js "statics/console2" "js/jquery.keyboard-mapping.js"/]
    [@nexus.js "statics/console2" "js/common.js"/]

    <script type="text/javascript" src="/p/ui/core/js/Microsoft.Xbox.WinJS.1.0/js/base.js"></script>
    <script type="text/javascript" src="/p/ui/core/js/Microsoft.Xbox.WinJS.1.0/js/ui.js"></script>
    <script type="text/javascript" src="/p/ui/core/js/Microsoft.Xbox.WinJS.1.0/js/xbox.js"></script>


    [#if javascripts?has_content]
        [#include "${javascripts}"/]
    [/#if]
    [#if stylesheets?has_content]
        [#include "${stylesheets}"/]
    [/#if]

    [@googleanalytics/]
    [#--[@tealium/]--]

    [@afterLoading/]
</head>
<body class="${firstPartyUserProfile.namespace?string} [#if firstPartyUserProfile.swapXO]swap_xo[/#if]">
<div id="loading-container"></div>
<div id="transBackground"></div>
    [#nested /]

[#if nexfn.config("portal.show.buildversion")?eval]
<div class="version">v250.0.1-SNAPSHOT-20131025-1415</div>
[/#if]
<!-- ### ${hostName} ### -->
</body>
</html>
[/#macro]

[#macro logo]
<div class="originLogo"></div>
[/#macro]

[#macro rightPanel]
<div class="right-panel">
    [#if firstPartyUserProfile.namespace?starts_with("XBOX")]
        <p class="right-copy-1">[@nexus.message 'console' 'RIGHT_SIDE_GRAPHIC_TEXT_LINE1_XBOX'/]</p>
        <p class="right-copy-2">[@nexus.message 'console' 'RIGHT_SIDE_GRAPHIC_TEXT_LINE2_XBOX'/]</p>
    [#else]
        <p class="right-copy-1">[@nexus.message 'console' 'RIGHT_SIDE_GRAPHIC_TEXT_LINE1_PS'/]</p>
        <p class="right-copy-2">[@nexus.message 'console' 'RIGHT_SIDE_GRAPHIC_TEXT_LINE2_PS'/]</p>
    [/#if]
</div>
[/#macro]

[#macro gameLogo]
<div class="gameLogo"></div>
[/#macro]

[#macro afterLoading]
<script type="text/javascript">
    function afterLoading() {
        document.body.style.zoom = window.screen.availWidth/1920;
        var sUserAgent = window.navigator.userAgent;
        if (sUserAgent.indexOf("EAWebKit") >= 0) $("body").addClass("console");
        $("#loading-container").css("display","none");
        $("#main-container").css("display","block").focus();
        if (window.parent) {
            window.parent.postMessage("frameloaded", "*");
        }
    }
</script>
[/#macro]

[#macro controlButton id label link='']
<div class="origin-ux-btn" id="${id}"[#if link?has_content] link="${link}"[/#if]>
    <span>${label}</span>
</div>
[/#macro]

[#macro googleanalytics]
<!--  GA -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '${nexfn.config('google_analytics_account')}']);
    _gaq.push(['_setDomainName', '${nexfn.config('google_analytics_domain')}']);
    _gaq.push(['_trackPageview']);
    (function () {
        var ga = document.createElement('script');ga.type = 'text/javascript';ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<!-- End of GA -->
[/#macro]

[#macro tealium]
    [#if nexfn.config("tealium.enabled")?eval]
    <!-- Tealium -->
    <script type="text/javascript">
        var utag_data = {
            region: "${nexfn.region()}",
            locale : "${nexfn.locale()}",
            country : "${nexfn.country()}",
            language : "${nexfn.lang()}",
            userid : "",
            user_status : "",
            referring_site : "${nexfn.referer()}",
            page_name : "${nexfn.requestUrl()}"
        }
    </script>
    <script type="text/javascript">
        (function(a,b,c,d){
            a='${nexfn.config("tealium.url")}';
            b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
            a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
        })();
    </script>
    <!-- End of tealium -->
    [/#if]
[/#macro]
