(function ($, undefined) {
//add test info
    $.fn.register = function (options, arguments) {
        var settings = {
            'tosLoadFailed': false,
            'contextPath': '',
            'regExEmail': /^[a-z0-9]+[a-z0-9!#$%&'*+/=?^_`{|}~-]*(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
            'strongPassword': /^.*(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(^[a-zA-Z0-9\[\]\`\!\@\#\$\%\^\&\*\(\)\_\=\{\}\:\;\<\>\+\-\']+$)/,
            'passwordBlacklist': ["password", "passw0rd", "qwerty", "monkey", "letmein", "trustno1", "dragon", "baseball", "iloveyou", "master", "sunshine", "ashley", "bailey", "shadow", "superman", "qazwsx", "michael", "football", "princess", "abc123", "111111", "111111111", "123123", "654321", "123456", "1234567","12345678", "123456789", "jesus", "mustang", "ninja", "password1", "welcome"],
            'console': $("body").hasClass("console"),
            'firstPartyUnderage': false,
            'forgetPassEmailResult' : '',
            'country' : ''
        };
        var timerID;
        var wheelSpeed = 50;
        var wheelDown = true;
        var lastScrollTop = 0;
        var allowBack = true;
        var methods = {
            init: function () {
                $.extend(settings, options);
                // ui event binding
                $(document).bind("keydown.create, xboxKeydown.create", methods.events.actionButtonKeydown);
                $(document).bind("xboxMouseWheel.create", methods.events.mouseWheel);
                $(".text-dlg").bind('xboxMouseWheel.create', methods.events.mouseWheelScroll);
                $(".security-question-options-container").bind('xboxMouseWheel.create', methods.events.mouseWheelScrollSecurity);
                $("#email").bind("blur.create", methods.events.validateEmail);
                $("#password").bind("blur.create", methods.events.validatePassword);
                $("#confirmPassword").bind("blur.create", methods.events.validateConfirmPassword);
                $("#originId").bind("blur.create", methods.events.validateOriginId);
                $("#securityAnswer").bind("blur.create", methods.events.validateSecurityAnswer);
                $("#recaptcha").bind("blur.create", methods.events.validateRecaptcha);
                $("#loginPassword").bind("blur.create", methods.events.validateLoginPassword);
                $(".btn-next").bind("click.create", methods.events.nextStep);
                $(".origin-ux-textbox-control input").unbind("blur.originUxElements");

                $("#email").attr("description", $("#email-tooltip").text());
                $("#email").attr("data-description", $("#email-tooltip").text());
                if ($("#error-message-title:not(:empty)").length > 0 || settings.tosLoadFailed) {
                    methods.helpers.showPanel("#panel-error-message");
                    $(".origin-ux-btn").hide();
                    $("#aBtn").show();$("#bBtn").show();
                } else if ($(".origin-ux-element").hasClass("field-error")) {
                    var errorId = "#" + $(".origin-ux-element.field-error").first().parents(".panel").attr("id");
                    _gaq.push(['_trackEvent', 'Console', 'Console LoginReg Error', errorId]);
                    methods.helpers.showPanel(errorId);
                    $(".origin-ux-element.field-error").setNucleusFocus();
                } else if (settings.forgetPassEmailResult != "") {
                    methods.helpers.showPanel("#panel-login");
                    if (settings.forgetPassEmailResult == "forget_pass_send_success") {
                        methods.helpers.showDialog($("#form-email-sent-title").text(), $("#form-email-sent-description").html());
                    } else if (settings.forgetPassEmailResult == "forget_pass_send_too_often") {
                        methods.helpers.showDialog($("#form-email-not-sent-title").text(), $("#form-email-sent-too-often").html());
                    } else {
                        methods.helpers.showDialog($("#form-email-not-sent-title").text(), $("#form-email-sent-failure").html());
                    }
                } else {
                    _gaq.push(['_trackEvent', 'Console', 'Console Legal Page']);
                    methods.helpers.showPanel("#panel-legal");
                }
            }, //END init
            events: {
                mouseWheel: function(event) {
                    var currentPanel = $(".panel:visible");
                    var dropDownOptions = currentPanel.find(".security-question-options-container");
                    if ($(this).is(":visible") && event) {
                        if (!dropDownOptions.is(":visible")) {
                            if (event.wheelDeltaY < -0.4) {
                                methods.helpers.moveUp();
                            } else if (event.wheelDeltaY > 0.4) {
                                methods.helpers.moveDown();
                            }
                        } else {
                            if (event.wheelDeltaY < -0.4) {
                                event.which = 38;
                            } else if (event.wheelDeltaY > 0.4) {
                                event.which = 40;
                            }
                            methods.helpers.scrollDropDown(dropDownOptions, event);
                        }
                    }
                },
                mouseWheelScroll: function(event) {
                    if ($(this).is(":visible")) {
                        if (timerID) clearInterval(timerID);
                        if (event && Math.abs(event.wheelDeltaX) < 0.4 && Math.abs(event.wheelDeltaY) < 0.4) {
                            wheelSpeed = 50;
                            wheelDown = true;
                        } else {
                            wheelDown = (event.wheelDeltaY > 0);
                            timerID = setInterval(methods.helpers.contentScroll, 100);
                        }
                    }
                },
                mouseWheelScrollSecurity: function(event) {
                    if ($(this).is(":visible")) {
                        if (timerID) clearInterval(timerID);
                        if (event) {
                            if (Math.abs(event.wheelDeltaX) < 0.4 && Math.abs(event.wheelDeltaY) < 0.4) {
                                wheelDown = true;
                            } else {
                                wheelDown = (event.wheelDeltaY > 0);
                                timerID = setInterval(methods.helpers.dropDownScroll, 300);
                            }
                        }
                    }
                },
                actionButtonKeydown: function (event) {
                    var currentPanel = $(".panel:visible");
                    var continueBtn = currentPanel.find("#continueBtn");
                    var dropDownOptions = currentPanel.find(".security-question-options-container");
                    if (!dropDownOptions.is(":visible")) {
                        switch (event.which) {
                            case 37: // left
                                break;
                            case WinJS.Utilities.Key.gamepadDPadUp: //38: // up
                                event.preventDefault();
                                methods.helpers.moveUp();
                                break;
                            case 39: // right
                                break;
                            case WinJS.Utilities.Key.gamepadDPadDown: //40: // down
                                event.preventDefault();
                                methods.helpers.moveDown();
                                break;
                        }
                    } else {
                        methods.helpers.scrollDropDown(dropDownOptions, event);
                        return;
                    }
                    if ($("input:focus").length == 0 || continueBtn.hasClass("focus")) {
                        switch (event.which) {
                            case WinJS.Utilities.Key.gamepadA: //112: // f1
                                event.preventDefault();
                                methods.helpers.aAction();
                                break;
                            case WinJS.Utilities.Key.gamepadB: //113: // f2
                                event.preventDefault();
                                methods.helpers.bAction();
                                break;
                            case WinJS.Utilities.Key.gamepadX: //114: // f3
                                event.preventDefault();
                                if ($("#xBtn").is(":visible")) {
                                    methods.helpers.showPanel("#panel-tos-text");
                                    $("#panel-tos-text").find(".text-dlg").scrollTop(0);
                                    $(".origin-ux-btn").hide();
                                    $("#bBtn").show();
                                }
                                break;
                            case WinJS.Utilities.Key.gamepadY: //115: // f4
                                event.preventDefault();
                                if ($("#yBtn").is(":visible")) {
                                    methods.helpers.showPanel("#panel-privacy-text");
                                    $("#panel-privacy-text").find(".text-dlg").scrollTop(0);
                                    $(".origin-ux-btn").hide();
                                    $("#bBtn").show();
                                }
                                break;
                        }
                    }
                }, // END actionButtonKeydown
                validateEmail: function () {
                    var valid = true;
                    var email = $("#email");
                    var continueBtn = email.parents(".panel").find("#continueBtn");
                    // initially disable the next button
                    continueBtn.addClass("disabled");
                    if (methods.helpers.validateIsEmpty(email)) {
                        $("#email-container .origin-ux-element, #email-container .origin-ux-control").removeClass("field-error").removeClass("field-validated");
                        valid = false;
                    }
                    // check if email is syntactically valid
                    if (valid && !settings.regExEmail.test(email.val().toLowerCase())) {
                        methods.helpers.errorState($(email));
                        email.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-invalid-email").text());
                        valid = false;
                    }
                    if (!valid) {
                        $("#email-container").addClass("field-error");
                        return;
                    }
                    var success = function (data, status) {
                        methods.events.isDuplicateEmail(data, status);
                    };
                    methods.helpers.callService("/user/checkEmailExisted", {success: success}, {email: email.val()});
                }, // END validateEmail
                isDuplicateEmail: function (data, status) {
                    var email = $("#email");
                    var continueBtn = email.parents(".panel").find("#continueBtn");
                    var emailStatusMessage = email.parents(".origin-ux-element").find(".origin-ux-status-message");
                    var emailControl = email.parents(".origin-ux-control");
                    var emailElement = email.parents(".origin-ux-element");
                    emailControl.removeClass("field-error").removeClass("field-validated");
                    emailElement.removeClass("field-error").removeClass("field-validated");
                    if (data.message == 'register_email_existed') {
                        $(".emailText").text($("#email").val());
                        $("#_eventId").val("login");
                        continueBtn.removeClass("disabled");
                        methods.helpers.moveDown();
                    } else if (data.message == 'register_email_invalid') {
                        emailControl.addClass("field-error");
                        emailElement.addClass("field-error");
                        emailStatusMessage.text($("#form-error-invalid-email").text());
                        continueBtn.addClass("disabled");
                    } else {
                        emailControl.addClass("field-validated");
                        emailElement.addClass("field-validated");
                        emailStatusMessage.html("&nbsp;");
                        $("#_eventId").val("create");
                        $(".emailText").text($("#email").val());
                        continueBtn.removeClass("disabled");
                        methods.helpers.moveDown();
                    }
                }, // END isDuplicateEmail
                validatePassword: function (event) {
                    var passwordCtrl = $("#password");
                    passwordCtrl.parents(".origin-ux-element").removeClass("field-validated").removeClass("field-error");
                    passwordCtrl.parents(".origin-ux-control").removeClass("field-validated").removeClass("field-error");
                    var password = passwordCtrl.val();
                    var email = $("#email").val();
                    var originId = $("#originId").val();
                    var valid = true;
                    if (!settings.strongPassword.test(password) || password.length > 16) {
                        methods.helpers.errorState(passwordCtrl);
                        passwordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-password-complexity").text());
                        valid = false;
                    }
                    if (valid && (jQuery.inArray(password.toLowerCase(), settings.passwordBlacklist) > -1 ||
                        (email != "" && email.toLowerCase().indexOf(password.toLowerCase()) > -1) ||
                        (originId != "" && originId.toLowerCase() == password.toLowerCase()))) {
                        methods.helpers.errorState(passwordCtrl);
                        passwordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-password-security").text());
                        valid = false;
                    }
                    if (valid) {
                        passwordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").html("&nbsp;");
                        passwordCtrl.parents(".origin-ux-element").addClass("field-validated");
                        if (event) methods.helpers.moveDown();
                    }
                    if ($("#confirmPassword").val() != "") {
                        methods.events.validateConfirmPassword(null);
                    }
                    methods.helpers.resetBasicNextButton();
                    return true;
                }, //END validatePassword
                validateConfirmPassword: function (event) {
                    var confirmPasswordCtrl = $("#confirmPassword");
                    confirmPasswordCtrl.parents(".origin-ux-element").removeClass("field-validated").removeClass("field-error");
                    confirmPasswordCtrl.parents(".origin-ux-control").removeClass("field-validated").removeClass("field-error");
                    // check if password meets complexity requirements
                    if (confirmPasswordCtrl.val() == "") {
                        return;
                    }
                    if (confirmPasswordCtrl.val() == $("#password").val()) {
                        if ($("#password").parents(".origin-ux-control").hasClass("field-error")) {
                            methods.helpers.errorState(confirmPasswordCtrl);
                            confirmPasswordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-password-complexity").text());
                        } else {
                            confirmPasswordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").html("&nbsp;");
                            confirmPasswordCtrl.parents(".origin-ux-element").addClass("field-validated");
                            if (event) {
                                methods.helpers.moveDown();
                            }
                        }
                    } else {
                        methods.helpers.errorState(confirmPasswordCtrl);
                        if ($("#password").val() == "") {
                            confirmPasswordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-password-complexity").text());
                        } else {
                            confirmPasswordCtrl.parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-unmatched-confirm-password").text());
                        }
                    }
                    methods.helpers.resetBasicNextButton();
                },
                validateOriginId: function () {
                    if ($("#password").val() != "") {
                        methods.events.validatePassword(null);
                    }
                    if ($(this).val() == "") {
                        $("#origin-id-container .origin-ux-element, #origin-id-container .origin-ux-control").removeClass("field-error").removeClass("field-validated");
                        return;
                    }
                    var success = function (data, status) {
                        methods.events.isDuplicateOriginId(data, status);
                    };
                    methods.helpers.callService(
                        "/user/checkOriginId",
                        {
                            success: success
                        },
                        {originId: $(this).val()}
                    );
                }, // END validateOriginId
                isDuplicateOriginId: function (data, status) {
                    $("#origin-id-container .origin-ux-element, #origin-id-container .origin-ux-control").removeClass("field-error").removeClass("field-validated");
                    var originIdStatusMessage = $("#originId").parents(".origin-ux-element").find(".origin-ux-status-message");
                    originIdStatusMessage.html("&nbsp;");
                    var originIdControl = $("#originId").parents(".origin-ux-control");
                    var originIdElement = $("#originId").parents(".origin-ux-element");
                    if (data.status === false) {
                        nucleusSDKHandle("error");
                        if (data.message) {
                            switch (data.message) {
                                case 'origin_id_duplicated':
                                    originIdStatusMessage.text($("#form-error-origin-id-duplicate").text());
                                    originIdControl.addClass("field-error");
                                    originIdElement.addClass("field-error");
                                    break;
                                case 'origin_id_too_long':
                                    originIdStatusMessage.text($("#form-error-origin-id-too-long").text());
                                    originIdControl.addClass("field-error");
                                    originIdElement.addClass("field-error");
                                    break;
                                case 'origin_id_too_short':
                                    originIdStatusMessage.text($("#form-error-origin-id-too-short").text());
                                    originIdControl.addClass("field-error");
                                    originIdElement.addClass("field-error");
                                    break;
                                case 'origin_id_not_allowed':
                                    originIdStatusMessage.text($("#form-error-origin-id-invalid").text());
                                    originIdControl.addClass("field-error");
                                    originIdElement.addClass("field-error");
                                    break;
                                default:
                                    originIdStatusMessage.text($("#form-error-origin-id-invalid").text());
                                    originIdControl.addClass("field-error");
                                    originIdElement.addClass("field-error");
                            }
                        } else {
                            // if there's a general failure send them to the connection error page.
                            methods.helpers.showPanel("#panel-connection-error");
                        }
                        methods.helpers.resetBasicNextButton();
                        return true;
                    } else {
                        $("#originId").parents(".origin-ux-element").addClass("field-validated");
                        methods.helpers.resetBasicNextButton();
                        methods.helpers.moveDown();
                        return false;
                    }
                }, //END isDuplicateOriginId
                validateSecurityAnswer: function() {
                    $(this).parents(".origin-ux-element").removeClass("field-validated").removeClass("field-error");
                    $(this).parents(".origin-ux-control").removeClass("field-validated").removeClass("field-error");
                    if ($(this).val() == "") {
                        return;
                    }
                    if ($(this).val().length < 4 || $(this).val().length > 200) {
                        methods.helpers.errorState($(this));
                        $(this).parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-security-answer-too-short").text());
                    } else {
                        $(this).parents(".origin-ux-element").find(".origin-ux-status-message").html("&nbsp;");
                        $(this).parents(".origin-ux-element").addClass("field-validated");
                        methods.helpers.moveDown();
                    }
                },
                validateRecaptcha: function() {
                    methods.helpers.clearErrorMsg($(this));
                    if ($(this).val() != "") {
                        methods.helpers.moveDown();
                    }
                },
                validateLoginPassword: function() {
                    methods.helpers.clearErrorMsg($(this));
                    if ($(this).val() != "") {
                        methods.helpers.moveDown();
                        methods.helpers.moveDown();
                    }
                },
                validateRegistration: function() {
                    _gaq.push(['_trackEvent', 'Console', 'Console Registration']);
                    $("#_eventId").val("create");
                    $("#regForm").submit();
                },
                validateLogin: function() {
                    _gaq.push(['_trackEvent', 'Console', 'Console Login']);
                    $("#_eventId").val("login");
                    $("#regForm").submit();
                },
                nextStep: function() {
                    if ($("#modal").is(":visible")) {
                        nucleusSDKHandle("back");
                        $("#modal").hide();
                        $("#modalMask").hide();
                    } else {
                        var currentPanel = $(".panel:visible");
                        var continueBtn = currentPanel.find("#continueBtn");
                        currentPanel.find(".origin-ux-element .origin-ux-control").removeClass("focus");
                        continueBtn.setNucleusFocus();
                        methods.helpers.aAction();
                    }
                }
            }, // END events
            helpers: {
                aAction: function () {
                    var currentPanel = $(".panel:visible");
                    var continueBtn = currentPanel.find("#continueBtn");
                    var inputTextBox = currentPanel.find(".origin-ux-textbox.focus");
                    var securityQADropDownCtrl = currentPanel.find(".security-question-drop-down-control.focus");

                    if (inputTextBox.length > 0) {
                        inputTextBox.find("input").focus();
                    } else if (securityQADropDownCtrl.length > 0) {
                        securityQADropDownCtrl.addClass("selected");
                        securityQADropDownCtrl.find(".security-question-options-container").toggle();

                        var listDiv = securityQADropDownCtrl.find(".questionListBox");
                        listDiv.scrollTop(lastScrollTop);
                        nucleusSDKHandle("navigate");
                    } else {
                        switch (currentPanel.attr('id')) {
                            case 'panel-legal':
                                if ($("#readAccept").parents(".origin-ux-control").hasClass("focus")) {
                                    nucleusSDKHandle("check");
                                    $("#readAccept").trigger('click');
                                    if ($("#readAccept").prop("checked")) {
                                        continueBtn.removeClass("disabled");
                                        methods.helpers.moveDown();
                                    } else {
                                        continueBtn.addClass("disabled");
                                    }
                                } else if (continueBtn.hasClass("focus")) {
                                    _gaq.push(['_trackEvent', 'Console', 'Console Email Input']);
                                    methods.helpers.showPanel("#panel-email");
                                    methods.helpers.focusTextInput($("#email"));
                                    $("#xBtn, #yBtn").hide();
                                }
                                break;
                            case 'panel-email':
                                var nextPanelName;
                                if ($("#_eventId").val() == "login") {
                                    nextPanelName = "#panel-login";
                                } else {
                                    nextPanelName = "#panel-basic";
                                }
                                if (continueBtn.hasClass("focus")) {
                                    methods.helpers.showPanel(nextPanelName);
                                    $(nextPanelName).find("input:first").parents(".origin-ux-element").setNucleusFocus();
                                }
                                break;
                            case 'panel-basic':
                                if ($("#password").val() != "" && $("#confirmPassword").val() != "" && $("originId").val() != "" && currentPanel.find(".origin-ux-element.field-error").length == 0) {
                                    _gaq.push(['_trackEvent', 'Console', 'Console Security QA']);
                                    methods.helpers.showPanel("#panel-security-question");
                                    $("#securityqa").setNucleusFocus().parents(".origin-ux-element").setNucleusFocus();
                                }
                                break;
                            case 'panel-security-question':
                                if ($("#securityQuestion").val() != "" && $("#securityAnswer").val() != "" && currentPanel.find(".origin-ux-element.field-error").length == 0) {
                                    methods.helpers.preRenderContactPrefs();
                                    _gaq.push(['_trackEvent', 'Console', 'Console Preference']);
                                    methods.helpers.showPanel("#panel-preference");
                                }
                                break;
                            case 'panel-preference':
                                var checkBox = currentPanel.find(".origin-ux-checkbox-control.focus");
                                if (checkBox.length > 0) {
                                    nucleusSDKHandle("check");
                                    checkBox.find("input:visible").trigger('click');
                                } else {
                                    continueBtn.addClass("disabled");
                                    methods.events.validateRegistration();
                                }
                                break;
                            case 'panel-login':
                                if ($("#forgetPassBtn").hasClass("focus")) {
                                    _gaq.push(['_trackEvent', 'Console', 'Console Forgot Pass']);
                                    $("#_eventId").val("forgetPass");
                                    $("#regForm").submit();
                                } else if (continueBtn.hasClass("focus")) {
                                    if ($("#panel-captcha").length > 0) {
                                        _gaq.push(['_trackEvent', 'Console', 'Console Login Captcha']);
                                        methods.helpers.showPanel("#panel-captcha");
                                        $("#recaptcha").parents(".origin-ux-textbox").setNucleusFocus();
                                    } else {
                                        continueBtn.addClass("disabled");
                                        methods.events.validateLogin();
                                    }
                                }
                                break;
                            case 'panel-captcha':
                                if ($("#refreshCaptchaBtn").hasClass("focus")) {
                                    // refresh captcha click, its sound is "check"
                                    nucleusSDKHandle("check");
                                    $("#captchaImg").attr("src", settings.contextPath + "/captcha?" + Math.random());
                                } else if (continueBtn.hasClass("focus")) {
                                    if ($("#recaptcha").val() != "") {
                                        methods.events.validateLogin();
                                    }
                                }
                                break;
                            case 'panel-connection-error':
                            case 'panel-error-message':
                                methods.helpers.goBack();
                                break;
                        }
                    }
                },
                bAction: function () {
                    var currentPanel = $(".panel:visible");
                    switch (currentPanel.attr('id')) {
                        case 'panel-legal':
                            methods.helpers.goBack();
                            break;
                        case 'panel-email':
                            methods.helpers.showPanel("#panel-legal", true);
                            break;
                        case 'panel-basic':
                            methods.helpers.showPanel("#panel-email", true);
                            break;
                        case 'panel-login':
                            if ($("#modal").is(":visible")) {
                                nucleusSDKHandle("back");
                                $("#modal").hide();
                                $("#modalMask").hide();
                                currentPanel.find("input:first").parents(".origin-ux-element").setNucleusFocus();
                            } else {
                                methods.helpers.showPanel("#panel-email", true);
                            }
                            break;
                        case 'panel-security-question':
                            methods.helpers.showPanel("#panel-basic", true);
                            break;
                        case 'panel-preference':
                            methods.helpers.showPanel("#panel-security-question", true);
                            break;
                        case 'panel-tos-text':
                        case 'panel-privacy-text':
                            $(".origin-ux-btn").show();
                            methods.helpers.showPanel("#panel-legal", true);
                            break;
                        case 'panel-connection-error':
                        case 'panel-error-message':
                            methods.helpers.goBack();
                            break;
                        case 'panel-captcha':
                            methods.helpers.showPanel("#panel-login", true);
                            break;
                    }
                },
                moveUp: function () {
                    var currentPanel = $(".panel:visible");
                    var currentInput = currentPanel.find(".origin-ux-element.focus");
                    var currentContinueBtn = currentPanel.find("#continueBtn");
                    switch (currentPanel.attr('id')) {
                        case "panel-legal":
                            if (currentContinueBtn.hasClass("focus")) {
                                currentContinueBtn.removeClass("focus");
                                $("#readAccept").parents(".origin-ux-control").setNucleusFocus();
                            }
                            break;
                        case "panel-tos-text":
                        case "panel-privacy-text":
                            var textDlg = currentPanel.find(".text-dlg");
                            textDlg.scrollTop(textDlg.scrollTop() - 100);
                            break;
                        case "panel-email":
                            if (currentContinueBtn.hasClass("focus")) {
                                currentContinueBtn.removeClass("focus");
                                $("#email").parents(".origin-ux-textbox").setNucleusFocus();
                            }
                            break;
                        case "panel-basic":
                        case "panel-login":
                        case "panel-captcha":
                            if (currentInput.length > 0) {
                                var prevInput = currentInput.parents("li").prev().find(".origin-ux-element");
                                if (prevInput.length > 0) {
                                    currentPanel.find(".origin-ux-element").removeClass("focus");
                                    prevInput.setNucleusFocus();
                                } else if (currentContinueBtn.hasClass("focus")) {
                                    currentContinueBtn.removeClass("focus");
                                    currentPanel.find("li:last").find(".origin-ux-element").setNucleusFocus();
                                }
                                if ($("#forgetPassBtn").hasClass("focus")) {
                                    currentPanel.find("#forgetPassTooltip").show();
                                } else {
                                    currentPanel.find("#forgetPassTooltip").hide();
                                }
                            }
                            break;
                        case "panel-security-question":
                            if (currentInput.length > 0) {
                                var prevInput = currentInput.parents("li").prev().find(".origin-ux-element");
                                if (prevInput.length > 0) {
                                    if (!settings.console) currentInput.find("input").blur();
                                    currentPanel.find(".origin-ux-element").removeClass("focus");
                                    prevInput.setNucleusFocus();
                                    prevInput.find(".origin-ux-drop-down-control").setNucleusFocus();
                                } else {
                                    currentContinueBtn.removeClass("focus");
                                    $("#securityAnswer").parents(".origin-ux-element").setNucleusFocus();
                                }
                            }
                            break;
                        case "panel-preference":
                            var contactMeCheckbox = $("#contactMe").parents(".origin-ux-control");
                            var shareInfoCheckbox = $("#shareInfo").parents(".origin-ux-control");
                            if (currentContinueBtn.hasClass("focus")) { currentContinueBtn.removeClass("focus"); shareInfoCheckbox.setNucleusFocus().focus(); currentPanel.find("#shareInfoTooltip").css('display', 'inline-block');}
                            else if (shareInfoCheckbox.hasClass("focus")) { shareInfoCheckbox.removeClass("focus");contactMeCheckbox.setNucleusFocus().focus(); currentPanel.find("#shareInfoTooltip").hide();}
                            break;
                    }
                },
                moveDown: function () {
                    var currentPanel = $(".panel:visible");
                    var currentInput = currentPanel.find(".origin-ux-element.focus");
                    var currentContinueBtn = currentPanel.find("#continueBtn");
                    switch (currentPanel.attr('id')) {
                        case "panel-legal":
                            if (currentContinueBtn.hasClass('disabled') || currentContinueBtn.hasClass("focus")) {
                                return;
                            }
                            currentContinueBtn.setNucleusFocus();
                            $("#readAccept").parents(".origin-ux-control").removeClass("focus");
                            break;
                        case "panel-tos-text":
                        case "panel-privacy-text":
                            var textDlg = currentPanel.find(".text-dlg");
                            textDlg.scrollTop(textDlg.scrollTop() + 100);
                            break;
                        case "panel-email":
                            if (currentContinueBtn.hasClass('disabled') || currentContinueBtn.hasClass("focus")) {
                                return;
                            }
                            currentContinueBtn.setNucleusFocus();
                            $("#email").parents(".origin-ux-textbox").removeClass("focus");
                            break;
                        case "panel-basic":
                            if (currentInput.length > 0) {
                                var nextInput = currentInput.parents("li").next().find(".origin-ux-element");
                                if (nextInput.length > 0) {
                                    currentPanel.find(".origin-ux-element").removeClass("focus");
                                    nextInput.setNucleusFocus();
                                } else {
                                    var validatedInputs = currentPanel.find(".field-validated");
                                    if (validatedInputs.length == 3 && !currentContinueBtn.hasClass("focus")) {
                                        currentPanel.find(".origin-ux-element").removeClass("focus");
                                        currentContinueBtn.setNucleusFocus();
                                    }
                                }
                            }
                            break;
                        case "panel-login":
                        case "panel-captcha":
                            if (currentInput.length > 0) {
                                var nextInput = currentInput.parents("li").next().find(".origin-ux-element");
                                if (nextInput.length > 0) {
                                    currentPanel.find(".origin-ux-element").removeClass("focus");
                                    nextInput.setNucleusFocus();
                                } else {
                                    if (!currentContinueBtn.hasClass("focus")) {
                                        currentPanel.find(".origin-ux-element").removeClass("focus");
                                        currentContinueBtn.setNucleusFocus();
                                    }
                                }
                                if ($("#forgetPassBtn").hasClass("focus")) {
                                    currentPanel.find("#forgetPassTooltip").show();
                                } else {
                                    currentPanel.find("#forgetPassTooltip").hide();
                                }
                            }
                            break;
                        case "panel-security-question":
                            if (currentInput.length > 0) {
                                var nextInput = currentInput.parents("li").next().find(".origin-ux-element");
                                if (nextInput.length > 0) {
                                    currentPanel.find(".origin-ux-element").removeClass("focus");
                                    currentInput.find(".origin-ux-drop-down-control").removeClass("focus").removeClass("selected");
                                    nextInput.setNucleusFocus();
                                } else {
                                    var validatedInputs = currentPanel.find(".field-validated");
                                    if ($("#securityQuestion").val() != "" && validatedInputs.length == 1 && !currentContinueBtn.hasClass("focus")) {
                                        currentPanel.find(".origin-ux-element").removeClass("focus");
                                        currentInput.find(".origin-ux-drop-down-control").removeClass("focus").removeClass("selected");
                                        currentContinueBtn.setNucleusFocus();
                                    }
                                }
                            }
                            break;
                        case "panel-preference":
                            var contactMeCheckbox = $("#contactMe").parents(".origin-ux-control");
                            var shareInfoCheckbox = $("#shareInfo").parents(".origin-ux-control");
                            if (contactMeCheckbox.hasClass("focus")) { contactMeCheckbox.removeClass("focus");shareInfoCheckbox.setNucleusFocus().focus(); currentPanel.find("#shareInfoTooltip").css('display', 'inline-block');}
                            else if (shareInfoCheckbox.hasClass("focus")) {
                                if (currentContinueBtn.hasClass('disabled') || currentContinueBtn.hasClass("focus")) {
                                    return;
                                }
                                shareInfoCheckbox.removeClass("focus").blur();
                                currentContinueBtn.setNucleusFocus();
                                currentPanel.find("#shareInfoTooltip").hide();
                            }
                            break;
                    }
                },
                resetBasicNextButton: function() {
                    var currentPanel = $(".panel:visible");
                    var validatedInputs = currentPanel.find(".field-validated");
                    var currentContinueBtn = currentPanel.find("#continueBtn");
                    if (validatedInputs.length == 3) {
                        currentContinueBtn.removeClass("disabled");
                    } else {
                        currentContinueBtn.addClass("disabled");
                    }
                },
                showDialog: function(title, description) {
                    var modalDialog = $("#modal");
                    var windowHeight = $("#main-container").height();
                    var windowWidth = $("#main-container").width();
                    modalDialog.find(".title").text(title);
                    modalDialog.find(".description").html(description);
                    modalDialog.find("#backBtn").show();
                    $("#modalMask").show();
                    modalDialog.css({"top":(windowHeight - modalDialog.height())/2 +"px", "left":(windowWidth - modalDialog.width())/2 +"px", "display":"block"});
                },
                showPanel: function (panelName, fromBack) {
                    $(".panel").css("display", "none");
                    $(panelName).css("display", "block");
                    $(panelName).find(".origin-ux-element").removeClass("focus");
                    var currentContinueBtn =  $(panelName).find("#continueBtn");
                    if (fromBack) {
                        currentContinueBtn.setNucleusFocus();
                    }
                    $(".origin-ux-btn").hide();
                    $("#aBtn").show();$("#bBtn").show();
                    switch (panelName) {
                        case '#panel-legal':
                            $("#xBtn").show();$("#yBtn").show();
                            if ($("#readAccept").prop("checked")) {
                                $("#readAccept").parents(".origin-ux-control").removeClass("focus");
                                currentContinueBtn.removeClass("disabled").setNucleusFocus();
                            } else {
                                $("#readAccept").parents(".origin-ux-control").setNucleusFocus();
                                currentContinueBtn.addClass("disabled").removeClass("focus");
                            }
                            break;
                        case '#panel-email':
                            methods.events.validateEmail();
                            break;
                    }
                    if(fromBack)
                    {
                        nucleusSDKHandle("back");
                    }
                    else
                    {
                        nucleusSDKHandle("transition");
                    }
                },
                validateIsEmpty: function (input) {
                    if ($(input).attr("type") == "checkbox") {
                        if (!$(input).is(":checked")) {
                            var controlName = $(input).attr("name").replace('_', '-');
                            $(input).parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-missing-" + controlName).text());
                            $(input).parents(".origin-ux-control").addClass("field-error").parents(".origin-ux-element").addClass("field-error");
                            return true;
                        } else {
                            return false;
                        }
                    }
                    if ($(input).val() == "") {
                        var controlName = $(input).attr("name").replace('_', '-');
                        if (controlName.lastIndexOf("dob", 0) === 0) controlName = "dob";
                        $(input).parents(".origin-ux-element").find(".origin-ux-status-message").text($("#form-error-missing-" + controlName).text());
                        $(input).parents(".origin-ux-control").addClass("field-error").parents(".origin-ux-element").addClass("field-error");
                        return true;
                    } else {
                        return false;
                    }
                }, //END validateIsEmpty()
                errorState: function (input) {
                    nucleusSDKHandle("error");
                    $(input).parents(".origin-ux-control").removeClass("field-validated").addClass("field-error").parents(".origin-ux-element").addClass("field-error").removeClass("field-validated");
                }, // END errorState()
                callService: function (serviceName, handlers, data) {
                    var hostLoc = settings.contextPath + '/ajax';
                    $hr = $.ajax($.extend(handlers,
                        {
                            url: hostLoc + serviceName,
                            dataType: 'json',
                            timeout: 3000,
                            cache: false,
                            data: $.extend({ requestorId : "portal" }, data),
                            error: function (jqXHR, textStatus, errorThrown) {
                                nucleusSDKHandle("error");
                                methods.helpers.showPanel("#panel-connection-error");
                            }
                            //context: this
                        }));
                }, //END callService
                focusTextInput: function(textInput) {
                    if (textInput) {
                        textInput.parents(".origin-ux-element").setNucleusFocus();
                    }
                },
                dropDownScroll: function(){
                    var dropDownOptions = $(".panel:visible").find(".security-question-options-container");
                    var event = jQuery.Event("keydown");
                    event.which = wheelDown ? 40 : 38;
                    methods.helpers.scrollDropDown(dropDownOptions, event);
                },
                contentScroll: function(){
                    var currentContentDlg = $(".panel:visible .text-dlg");
                    var currentScrollTop = currentContentDlg.scrollTop();
                    wheelSpeed = wheelSpeed + 10;
                    if ((currentScrollTop > 0 && !wheelDown)) {
                        currentContentDlg.scrollTop(currentScrollTop - wheelSpeed);
                    } else if (wheelDown && currentScrollTop < (currentContentDlg.prop("scrollHeight") - currentContentDlg.height())) {
                        currentContentDlg.scrollTop(currentScrollTop + wheelSpeed);
                    } else {
                        wheelSpeed = 50;
                        wheelDown = true;
                        clearInterval(timerID);
                    }
                },
                scrollDropDown: function(dropDownOptions, event) {
                    event.preventDefault();
                    var currentElem = dropDownOptions.find("li.current");
                    var listDiv = dropDownOptions.find(".questionListBox");
                    var currentTop=listDiv.scrollTop();
                    switch (event.which) {
                        case WinJS.Utilities.Key.gamepadDPadUp: //38:
                            var listElemPrev = currentElem.prev();
                            if (listElemPrev.length > 0) {
                                currentElem.removeClass("current");
                                listElemPrev.addClass("current");
                                if (listElemPrev.position().top < 60) {
                                    listDiv.scrollTop(currentTop - 40);
                                }
                                methods.helpers.showArrows(listDiv.scrollTop());
                            }
                            break;
                        case WinJS.Utilities.Key.gamepadDPadDown: //40: // down
                            var listElemNext = currentElem.next();
                            if (listElemNext.length > 0) {
                                var nextTop = listElemNext.position().top;
                                currentElem.removeClass("current");
                                listElemNext.addClass("current");
                                if (nextTop > 240) {
                                    listDiv.scrollTop(currentTop + 40);
                                }
                                methods.helpers.showArrows(listDiv.scrollTop());
                            }
                            break;
                        case WinJS.Utilities.Key.gamepadA: //112:
                            nucleusSDKHandle("navigate");
                            $("#securityQuestion").val(currentElem.text());
                            $(".security-question-drop-down-selection span").text(currentElem.text());
                            $(".security-question-drop-down-control").removeClass("selected");
                            lastScrollTop = currentTop;
                            dropDownOptions.hide();
                            methods.helpers.moveDown();
                            break;
                        case WinJS.Utilities.Key.gamepadB:
                            $(".security-question-drop-down-control").removeClass("selected");
                            lastScrollTop = currentTop;
                            dropDownOptions.hide();
                            break;
                    }
                },
                showArrows: function(top) {
                    if (top > 0) {
                        $(".upArrow").show();
                    } else {
                        $(".upArrow").hide();
                    }
                    if (top <= 7*40) {
                        $(".downArrow").show();
                    } else {
                        $(".downArrow").hide();
                    }
                },
                preRenderContactPrefs: function() {
                    // Get the user's country
                    var country = settings.country;
                    country = country.toUpperCase();
                    // Decide whether or not to check the 'Email me about...' checkbox
                    var checkedByDefault = false;
                    if (jQuery.inArray(country, $.fn.register.globalSettings.emailMeAboutPreCheckedCountries) > -1) {
                        checkedByDefault = true;
                }
                    // Check/Un-check the input type='checkbox'
                    var identifier = "#contactMe";
                    $(identifier).prop('checked', checkedByDefault);
                    if (checkedByDefault) {
                        // Set the style for a checked checkbox.
                        $(identifier).parents(".origin-ux-control").addClass("checked");
                        // Set the focus to the 2nd checkbox (3rd Party Partners)
                        var shareInfoCheckbox = $("#shareInfo").parents(".origin-ux-control");
                        shareInfoCheckbox.addClass("focus").focus();
                    } else {
                        // Set the focus to the 1st checkbox (Email me About...)
                        $("#contactMe").parents(".origin-ux-control").addClass("focus");
                    }
                },
                clearErrorMsg: function(ctrl) {
                    if (ctrl.val() != "") {
                        ctrl.parents(".origin-ux-element").removeClass("field-validated").removeClass("field-error");
                        ctrl.parents(".origin-ux-control").removeClass("field-validated").removeClass("field-error");
                        ctrl.parents(".origin-ux-element").find(".origin-ux-status-message").html("&nbsp;");
                    }
                },
                goBack: function() {
                    if (allowBack) {
                        allowBack = false;
                        window.location = window.location + "&_eventId=back";
                    }
                }
            } // END helpers
        };// END: methods
        // initialization
        var publicMethods = [ "init" ];
        if (typeof options === 'object' || !options) {
            methods.init(); // initializes plugin
        } else if ($.inArray(options, publicMethods)) {
            // call specific methods with arguments
        }
    };  // END: $.fn.pluginName = function() {

    $.fn.register.globalSettings = {
        'emailMeAboutPreCheckedCountries': ["CA","MX","US","PR","AT","BE","BG","CY","CZ","DK","EE","FI","FR","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE","GB"]
    };

})(jQuery); // END: (function( $ ){

